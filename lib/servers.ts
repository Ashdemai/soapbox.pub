import domains from '../servers.json';

type Instance = Record<string, unknown>;

interface Server {
  title: string
  domain: string
  userCount: number
  description: string
}

const truncate = (string: string, max: number) => {
  if (string.length > max) {
    return string.slice(0, max) + '…';
  } else {
    return string;
  }
};

const instanceToServer = (instance: Instance, domain: string): Server => {
  const stats = instance.stats as Record<string, unknown> | undefined;

  return {
    title: typeof instance.title === 'string' ? instance.title : '',
    userCount: typeof stats?.user_count === 'number' ? stats.user_count : 0,
    description: typeof instance.description === 'string' ? truncate(instance.description, 80) : '',
    domain,
  };
};

const fetchServers = async(): Promise<Server[]> => {
  const responses = domains.map(async domain => {
    try {
      const response = await fetch(`https://${domain}/api/v1/instance`);
      return instanceToServer(await response.json(), domain);
    } catch (e) {
      console.error(`Failed to fetch server: ${domain}`);
      console.error(e);
      return undefined;
    }
  });

  const results = await Promise.all(responses);
  return results.filter(Boolean);
};

export {
  fetchServers,
};

export type {
  Server,
};