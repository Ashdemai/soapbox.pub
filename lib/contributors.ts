import { Gitlab, Types as GitlabTypes } from '@gitbeaker/node';

const { GITLAB_API_TOKEN } = process.env;

const api = new Gitlab({
  host: 'https://gitlab.com',
  oauthToken: GITLAB_API_TOKEN,
});

/** GitLab IDs of known bot accounts (for filtering out of results). */
const BOT_IDS = [
  11840292, // Soapbox Bot
];

/** Entry from GitLab's undocumented graph API. */
interface GraphCommit {
  /** Commit message author display name. */
  author_name: string
  /** Commit message author email. */
  author_email: string
  /** Date in the format `YYYY-MM-DD`. */
  date: string
}

/** Git author information. */
type Committer = Pick<GraphCommit, 'author_name' | 'author_email'>;

/**
 * Fetch 6,000 most recent commits.  
 * Undocumented API. Used on this page: https://gitlab.com/soapbox-pub/soapbox/-/graphs/main?ref_type=heads
 */
const fetchGraph = async(): Promise<GraphCommit[]> => {
  const response = await fetch('https://gitlab.com/soapbox-pub/soapbox/-/graphs/main?format=json&ref_type=heads');
  return await response.json();
};

/** Return the top contributing emails. */
const getTopEmails = (graph: GraphCommit[]): string[] => {
  type Stats = Record<string, number>;

  const stats = graph.reduce<Stats>((result, commit) => {
     result[commit.author_email] = (result[commit.author_email] || 0) + 1;
     return result;
  }, {});

  const entries = Object.entries(stats);

  return entries
    .sort((a, b) => b[1] - a[1])
    .map(entry => entry[0]);
};

/** Pulls committers out of the graph for each email. */
const committersFromEmails = (graph: GraphCommit[], emails: string[]): Committer[] => {
  return emails.map(email => {
    const { author_name, author_email } = graph.find(c => c.author_email === email);

    return {
      author_name,
      author_email,
    };
  });
};

/** Convert committers to members. */
const membersFromCommitters = (
  committers: Committer[],
  members: GitlabTypes.MemberSchema[],
): GitlabTypes.MemberSchema[] => {
  return committers.map(committer => {
    // HACK: Match on display name!!!
    // Which is super janky, but GitLab's API doesn't have any better way.
    // Fact is, it's good enough. And we trust project members.
    return members.find(member => member.name.toLowerCase() === committer.author_name.toLowerCase());
  });
};

/** Return an array of members where duplicates are removed. */
const deduplicateMembers = (members: readonly GitlabTypes.MemberSchema[]): GitlabTypes.MemberSchema[] => {
  const map = members.reduce((result, member) => {
    if (member?.id) {
      return result.set(member.id, member);
    } else {
      return result;
    }
  }, new Map<number, GitlabTypes.MemberSchema>());

  return Array.from(map.values());
};

/** Deduplicate string IDs. */
const deduplicateIds = (ids: string[]): string[] => {
  return Array.from(new Set(ids));
};

type Opts = {
  exclude?: number[]
}

/** Get top contributors on GitLab using some hackery. */
const getTopContributors = async(top = 3, commits = 250, opts: Opts = {}): Promise<GitlabTypes.MemberSchema[]> => {
  if (!GITLAB_API_TOKEN) {
    console.warn('GITLAB_API_TOKEN not found! Some parts of the website will not be displayed properly.');
    return [];
  }
  /** All GitLab project members. */
  const members = await api.ProjectMembers.all('17765635', {
    includeInherited: true,
    skip_users: BOT_IDS.concat(opts?.exclude).filter(Boolean),
  });
  /** 6,000 most recent commits. */
  const graph = await fetchGraph();
  /** 100 most recent commits. */
  const recent = graph.slice(0, commits);

  /**
   * Top emails from commits.
   * Prefer recent commits, but fallback to all-time.
   */
  const emails = deduplicateIds(
    getTopEmails(recent)
      .concat(getTopEmails(graph)),
  );

  /** Convert emails to committers. */
  const committers = committersFromEmails(graph, emails);

  // Convert committers to members.
  return deduplicateMembers(membersFromCommitters(committers, members))
    .slice(0, top);
};

/** Get number of commits for last X days. */
const getCommitFrequency = async(days = 90): Promise<number[]> => {
  const graph = await fetchGraph();
  
  type Stats = Record<string, number>;

  const stats = graph.reduce<Stats>((result, commit) => {
     result[commit.date] = (result[commit.date] || 0) + 1;
     return result;
  }, {});

  const entries = Object.entries(stats);

  return entries
    .sort((a, b) => (new Date(b[0])).valueOf() - (new Date(a[0])).valueOf())
    .map(entry => entry[1])
    .slice(0, days);
};

export {
  getTopContributors,
  getCommitFrequency,
};