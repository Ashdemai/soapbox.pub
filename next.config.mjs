// @ts-check
import fs from 'fs';

import { parseNetlifyRedirects } from '@hashicorp/netlify-to-nextjs-redirect-exporter';
import nextMDX from '@next/mdx';
import { withContentlayer } from 'next-contentlayer';

// Parse `_redirects` so the local dev server respects it.
const contents = fs.readFileSync('public/_redirects', 'utf8');
const { rewrites, redirects } = parseNetlifyRedirects(contents);

// https://nextjs.org/docs/advanced-features/using-mdx
const withMDX = nextMDX({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [],
    rehypePlugins: [],
    providerImportSource: '@mdx-js/react',
  },
});

/** @type {import('next').NextConfig}*/
const nextConfig = {
  trailingSlash: true,
  rewrites: async() => rewrites,
  redirects: async() => redirects,
  images: {
    unoptimized: true,
  },
  pageExtensions: ['ts', 'tsx', 'js', 'jsx', 'md', 'mdx'],
  webpack: config => {
    config.module.rules.push(
      {
        test: /\.mmd$/,
        type: 'asset/source',
      },
    );
    return config;
  },
};

export default withContentlayer(withMDX(nextConfig));