---
title: Soapbox FE v1.1 Released
excerpt: This release is so solid it makes v1.0 feel unfinished. Truly, Soapbox FE was put to the ultimate test — a real social network with 14,000 users. 185 issues were closed. Major new features were added. This update brings us up to speed with the latest Pleroma, released last month.
coverImage: '/assets/blog/soapbox-fe-v1.1-release/v-1-1-thumb.png'
date: '2020-10-05'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/soapbox-fe-v1.1-release/v-1-1-thumb.png'
---

## What is Pleroma?

Pleroma is a free and open source, federated, social media software allowing anyone to host their own online community. Servers can cross-communicate with other servers on the network thanks to federation. Soapbox&nbsp;FE is a sleek user interface that server admins can install on top of Pleroma.

## Quality of Life

This release is so solid it makes v1.0 feel unfinished. Truly, Soapbox FE was put to the ultimate test — *a real social network with 14,000 users.*

185 issues were closed. Major new features were added. This update brings us up to speed with [the latest Pleroma, released last month](https://pleroma.social/blog/2020/08/28/releasing-pleroma-2-1-0/).

## The Big Migration

Spinster, the largest feminist server on the network, has now [been migrated](https://4w.pub/spinster-2-0-moving-from-mastodon-backend-to-pleroma/) from Mastodon to Pleroma. This was a difficult challenge that pushed us to the limit.

Fortunately, our theory was correct, and Spinster now requires a fraction of the resources to operate. Before and after:

![](/assets/blog/soapbox-fe-v1.1-release/spinster-mastodon-to-pleroma-comparison.png)

Having a real userbase this time made all the difference. Users quickly identified bugs, and motivated us to make the software more robust to accomodate such a large community.

Spinster is not the only site migrated to Pleroma+Soapbox this round. Community member Anirudh Oppiliappan was the first outsider to successfully migrate Mastodon to Pleroma using our tool, and [his blog post](https://icyphox.sh/blog/mastodon-to-pleroma/) hit the front page of Hacker News. Congrats Anirudh!

## Chats

Without a doubt, Pleroma's biggest new feature is federated chats. Chats are private and 1-on-1. This exciting new way to communicate is only available on some backends, so you'll want your friends to be on Pleroma too.

![](/assets/blog/soapbox-fe-v1.1-release/soapbox-chats.gif)

"Mobile First" is the norm nowadays, but I think the Fediverse really needs a Facebook-style chat interface. So I built both. On desktop you can chat while browsing, and on mobile the chat will fill the screen.

![](/assets/blog/soapbox-fe-v1.1-release/chat-panes.png)

## Soapbox Config

A big promise of Soapbox has always been server customization. As an admin, you're expected to bring your own brand. In older versions of Soapbox this required manually editing a file on the server, but now site settings such as logo and brand color can be customized right through the GUI.

![](/assets/blog/soapbox-fe-v1.1-release/soapbox-config.gif)

**Upgrade note:** If you've already configured a soapbox.json file, simply load the config page once and hit "Save". This will move your soapbox.json into the database, and you can safely delete it from your server.

## Profile Hover Cards

A classic Twitter feature, it took far too long for this to hit the Fediverse. Just hover your mouse over a user to see a preview of their account.

![](/assets/blog/soapbox-fe-v1.1-release/profile-hover-card.gif)

## Theme Toggle

To protect your eyes, you can now quickly switch themes. In direct sunlight you'll likely need light mode to see the screen at all. While browsing at night, dark mode is easier on the eyes.

![](/assets/blog/soapbox-fe-v1.1-release/theme-toggle.gif)

## Markdown

Posts are now Markdown-enabled by default! This will enable rich text formatting, and can be easily disabled on a per-post basis.

![](/assets/blog/soapbox-fe-v1.1-release/markdown.png)

## Instance Favicons

In order to help newcomers understand the Fediverse, instance favicons can be shown next to each post. This feature needs to be enabled in Pleroma settings before it will take effect.

![](/assets/blog/soapbox-fe-v1.1-release/favicons.png)

This is the first step in an initiative to "gameify" the Fediverse, and make its unique qualities truly shine.

## Import Data

Probably the most highly requested feature since the v1.0 release, users want to import their follows when moving from Mastodon to a Pleroma+Soapbox server. CSV exports from Mastodon can now be imported into Soapbox FE.

![](/assets/blog/soapbox-fe-v1.1-release/import-data.png)

## Bookmarks

To easily revisit posts later, they can be bookmarked and revisited any time.

![](/assets/blog/soapbox-fe-v1.1-release/bookmarks.png)

## Audio Player

Uploading sound clips displays an embedded audio player. Uploading multiple files will display the audio player in a modal window.

![](/assets/blog/soapbox-fe-v1.1-release/audio-player.png)

## Timeline Filters

More advanced timeline filtering options now exist, such as filtering out DMs.

![](/assets/blog/soapbox-fe-v1.1-release/timeline-filters.png)

## Word Filters

Certain words or phrases can be filtered out of your timeline.

![](/assets/blog/soapbox-fe-v1.1-release/word-filters.png)

## Multi-Factor Auth

For improved security, users can enable multi-factor auth. After setup, this will prompt them to enter a time-based code from an OTP app on their phone whenever they log in.

![](/assets/blog/soapbox-fe-v1.1-release/mfa.png)

## Layout Improvements

Space was optimized in this release so all three columns are used.

![](/assets/blog/soapbox-fe-v1.1-release/layout.png)

## Recurring Donations (Experimental)

You may have noticed the "Donation" widget in these screenshots. That comes from [Patron](https://gitlab.com/soapbox-pub/patron), an experimental recurring donations platform. It's already running in production on a few servers, but isn't ready for broader adoption yet.

![](/assets/blog/soapbox-fe-v1.1-release/patron.png)

Our end goal for this is to allow servers to be self-funded by their users.

## Bug Fixes

Some pretty annoying bugs were fixed. In particular there were some deep issues with scrollable lists and the post composer that got resolved.

- **Post Composer** — When the autosuggest box popped up, the cursor would jump to the end no matter what. This was fixed by preventing unnecessary re-renders of the autosuggest textarea.
- **Follower lists** — While scrolling follower lists, entries would render invisibly and jump around the screen. This was due to duplicate entries and was resolved by storing the entries in a Set instead of a List.
- **Notifications** — Same as above. Duplicate notifications were prevented by using an OrderedMap instead of a List.
- **Bundle size** — Unnecessary libraries were removed or optimized, and thousands of lines of unused CSS were deleted. Because of this the bundle size stayed roughly the same despite all the new features.

View the [CHANGELOG](https://gitlab.com/soapbox-pub/soapbox/-/blob/main/CHANGELOG.md) for the complete list of bug fixes.

## Happy Halloween!

One of the few missing features from Soapbox Legacy has returned in this release, just in time for Halloween. 🎃 Enable it in your Preferences.

<video src="/assets/blog/soapbox-fe-v1.1-release/halloween.mp4" autoPlay loop muted></video>

## In the Future

Just a small preview of a few ideas we're working on for a later release:

- **Viewing remote timelines** — clicking an instance favicon will show you a timeline of that server's posts. This is part of an initiative to "gameify" the Fediverse.
- **Page editor** — we want to add basic CMS capabilities so you can manage a wiki on your server directly through Soapbox FE.
- **Record your voice** — speak directly into the post composer to send an audio message.

Have an idea? Submit it to our [issue tracker](https://gitlab.com/soapbox-pub/soapbox/-/issues).

## Installing Soapbox FE

Installation on an existing Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.

To upgrade, repeat these steps.

### 0. Install Pleroma

If you haven't already, follow [this guide](https://docs-develop.pleroma.social/backend/installation/debian_based_en/) to install Pleroma. If you're still running an old Pleroma version, be sure to upgrade it <em>before</em> upgrading Soapbox FE.

### 1. Fetch the v1.1 build

```sh
curl -L https://gitlab.com/soapbox-pub/soapbox/-/jobs/artifacts/v1.1.0/download?job=build-production -o soapbox-fe.zip
```

### 2. Unpack

```sh
busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance
```

*Or, if you installed the OTP release, unpack to this path instead:*

```sh
busybox unzip soapbox-fe.zip -o -d /var/lib/pleroma
```

**That's it! 🎉 Soapbox FE is installed.**
The change will take effect immediately, just refresh your browser tab.
It's not necessary to restart the Pleroma service.

Note that it replaces Pleroma FE. (Running alongside Pleroma FE is [also possible](https://docs.soapbox.pub/frontend/administration/install-subdomain/)). Logged-in users will have to re-authenticate.

For **removal**, run: `rm /opt/pleroma/instance/static/index.html`

### 3. Customize it

After logging in, click your avatar in the topbar, and navigate to Soapbox Config to customize it. Additional settings can be found under Admin Settings in the same menu.

![](/assets/blog/soapbox-fe-v1.1-release/admin-link.png)

## Fund the Soapbox Project

Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts. Thank you!

- Credit Card — [PayPal](https://paypal.me/gleasonator) (alex@alexgleason.me)
- Bitcoin — `bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n`
- Etherium — `0xAc9aB5Fc04Dc1cB1789Af75b523Bd23C70B2D717`
- Dogecoin — `D5zVZs6jrRakaPVGiErkQiHt9sayzm6V5D`
- Ubiq (10grans) — `0x541a45cb212b57f41393427fb15335fc89c35851`
- Monero — `45JDCLrjJ4bgVUSbbs2yjy9m5Mf4VLPW8fG7jw9sq5u69rXZZopQogZNeyYkMBnXpkaip4p4QwaaJNhdTotPa9g44DBCzdK`

## Contributors

A HUGE thank you to everyone who helped make this release a reality! In particular I'd like to thank M.K. Fain and the Spinster community for pushing to make this release perfect. And of course, the usual suspects. 😉

- Curtis Rock
- Sean King
- Bárbara de Castro Fernandes
- M.K. Fain
- Lain, Feld, and the Pleroma dev team!

To everyone else I did not mention, a huge thank you for being part of this journey! I'm eternally grateful, and excited about how this community is growing.

## Stay Updated

To keep updated, follow me on the Fediverse at [@alex@gleasonator.com](https://gleasonator.com/users/alex). Thanks for your support!
