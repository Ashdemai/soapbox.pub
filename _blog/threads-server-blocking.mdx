---
title: Threads is blocking servers on the Fediverse. Here's how we unblocked ourselves.
excerpt: Threads has begun testing federation over ActivityPub. And they have blocked two important servers I administrate. The first server is the Mostr Bridge. The Mostr Bridge connects ActivityPub and Nostr together, so people can communicate across protocols. The second server is Spinster.xyz, which is arguably the largest independent feminist community online.
coverImage: '/assets/blog/facebook-hoodie.jpg'
date: '2023-12-25'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/facebook-hoodie.jpg'
---

Six years ago I deleted my Facebook and jumped ship to Mastodon. I wanted the Fediverse to win, and I always believed the way was to force Facebook and others to federate as their only option to stay relevant.

Finally my dream has been realized, somewhat. Threads has begun testing federation over ActivityPub. But to my dismay, I've discovered that Threads is already blocking a handful of servers, including several sites I administrate. Today I am proud to announce that we are fighting back.

## Blocked servers

At least 5 servers are known to be blocked by Threads:

- `mostr.pub`
- `spinster.xyz`
- `neenster.org`
- `poa.st`
- `mitra.social`

### The Mostr Bridge

The first blocked server is the [Mostr Bridge](/blog/mostr-fediverse-nostr-bridge/). The Mostr Bridge connects ActivityPub and Nostr together, so people can communicate across protocols. Blocking this bridge amounts to blocking Nostr as a whole from communicating with Threads, a major blow to interoperability between protocols.

Nostr has grown significantly in the past year and has been [backed by Jack Dorsey](https://opensats.org/blog/opensats-receives-additional-funding-of-dollar10m-from-startsmall). It's interesting that Meta has chosen to silence a worthy competitor, raising questions about their motives for federating.

<iframe
  title="The Mostr Bridge | Alex Gleason | Nostrasia 2023"
  width="560"
  height="315"
  src="https://poast.tv/videos/embed/99131b87-46b8-4b1f-bc8c-a019892f933f"
  frameborder="0"
  allowFullScreen=""
  sandbox="allow-same-origin allow-scripts allow-popups"
  className='shadow rounded-xl max-w-full'
></iframe>

### Spinster

[Spinster](https://spinster.xyz/) is the largest feminist server running on Soapbox, and is arguably the largest independent feminist community online. It is one of the sites blocked by Threads.

What exactly is Threads afraid of? Do we need to report this violation to [Meta's DEI department](https://www.metacareers.com/diversity)?

![Spinster blocked](/assets/blog/spinster-before.png)

### Neenster

Oddly enough, free-culture artist and animator [Nina Paley](https://blog.ninapaley.com/) has been blocked. Between blocking her account on Spinster and blocking [her personal server](https://neenster.org/), it appears that Meta has it out for Nina Paley specifically.

I've heard that Nina has some pretty dangerous ideas, but I'm curious which it was that crossed the line? Was is [Seder-Macochism](https://sedermasochism.com/)? Or [Sita Sings the Blues](https://www.sitasingstheblues.com/)? I would love to know the real reason.

![Nina Paley](/assets/blog/nina-paley.jpg)

### Poast

[Poast](https://poa.st/) is a loosely-moderated shitposting community, and the largest Soapbox server online. Between its raunchy jokes and political incorrectness, it is the posterchild of everything Meta's Terms of Service stands against. Nevertheless, the decision to make a sweeping block against this whole server is truly lazy on Meta's part, as Meta is fully capable of moderating each user of Poast individually. We'll delve into that topic a bit later.

### Mitra

[Mitra](https://mitra.social/) is an ActivityPub backend written in Rust, with Ethereum and Monero integrations. Its flagship server `mitra.social` appears to be blocked by Threads, [according to its developer](https://mitra.social/post/018c6a15-a34b-a462-7ca0-e124e8f78cf9).

Mitra's developer silverpill has authored [numerous Fediverse specs](https://codeberg.org/fediverse/fep/pulls?type=all&sort=&state=closed&labels=&milestone=0&project=0&assignee=0&poster=29663), making it especially interesting that Meta has targeted a prolific author of Fediverse standards.

## Are they really blocking us? Why?

In our tests, we are consistently unable to fetch from Threads using the above domains. But without transparency from Meta, it's impossible to know whether the blocks are intentional.

![Contacting Mosseri](/assets/blog/threads-contact-mosseri.png)

![Contacting btsavage](/assets/blog/threads-contact-btsavage.png)

I have reached out to the Threads team, and they did not respond. They have not published an official list of servers they block. Without full transparency, I am unable to know if they are intentionally blocking these servers. I am therefore assuming that this is a bug on their end. And I'm proud to announce that I have fixed this bug, by signing requests from a different domain.

## Fighting back

Starting today, the Mostr Bridge, Spinster, and others are now able to federate with Threads, at least to the extent Threads currently allows. Users of those sites can now discover users on Threads and view their posts!

<figure>
  <div className='grid grid-cols-2 gap-2'>
    <img src='/assets/blog/damus-before.png' />
    <img src='/assets/blog/damus-after.png' />
  </div>
  <figcaption>Damus before and after federating with Threads</figcaption>
</figure>

**Users on Nostr can now follow users on Threads, eg:** [@mosseri_at_threads.net@mostr.pub](https://nostr.com/p/2e0d5aab3fedffd4ad9456bbbc207bf22b0e3d6b4b26924a0a48652067e68f7d)

Server admins running Rebased will need to [apply a patch](https://gitlab.com/soapbox-pub/rebased/-/snippets/3634512) and configure a separate domain to sign requests. **Spinster, Neenster, and Poast have already all been updated, so users of those sites can now interact with Threads freely.**

<figure>
  <div className='grid grid-cols-2 gap-2'>
    <img src='/assets/blog/spinster-before.png' />
    <img src='/assets/blog/spinster-after.png' />
  </div>
  <figcaption>Spinster before and after federating with Threads</figcaption>
</figure>

Threads is operating in limited federation mode with only about 5 accounts exposed. They cannot see our posts, but we can see theirs. Previously other Mastodon servers could see posts from Threads, while the aforementioned servers could not. After the fix we're back on equal footing.

I find it interesting that _Meta Platforms, Inc._, a company known for harvesting user data, is blocking some servers from fetching its public posts. They decided to implement a feature Mastodon calls _Authorized fetch_. The feature already existed on the Fediverse, but it was [never offically enshrined](https://github.com/w3c/activitypub/issues/402).

## How we got around Authorized fetch

Authorized fetch was [introduced by Mastodon in 2019](https://github.com/mastodon/mastodon/pull/11269). In theory, it would prevent blocked users on other sites from seeing your posts. It would also prevent entire blocked servers from seeing your posts.

Since ActivityPub is a fundamentally open network, there are of course ways to circumvent it, some of which are very easy. You can create a new domain, fetch data with it, and then transfer the data to your main domain.

And that is exactly what I did. My modifications of [Rebased](https://gitlab.com/soapbox-pub/rebased/-/commit/2eb5c453ad71417bebcc45714c0c318bc956c336) and [Mostr](https://gitlab.com/soapbox-pub/mostr/-/commit/cecee9ed129c9ce99386421205f09ebe806d9b90) allow the admin to configure a separate domain to sign requests. If the domain gets blocked, it can be easily switched out for a new one without impacting the main site.

**Meta seems to be betting on the fact that people have played nicely in the past, but I for one am not going to let them have their way.** I am going to ensure the data they publish remains free and open to all.

**However, if Meta publishes a transparency page listing all the servers they block, I will stop "fixing" the federation issues between those servers.**

Tools to work around Authenticated fetch are being shipped with new versions of Fediverse software. Censorship by Meta will create a continued need for this industry to grow.

## How to tell if your server is blocked

If you're a server administrator, try visiting a known Threads account such as `@mosseri@threads.net` from your server. If the profile is unavailable, check your server logs for requests from `facebookexternalua`, the user-agent used by Threads:

```sh
grep facebookexternalua /var/log/nginx/access.log
```

- If you **do** see requests from `facebookexternalua` but you cannot load profiles from Threads, it's likely a bug in your server. Try upgrading.

- If you **do not** see requests from `facebookexternalua` in your logs, and you cannot load profiles from Threads, your server might be blocked, ruling out any other issues with your server such as firewall rules or signed fetching being disabled.

Authorized fetch is a two-step process. First your server identifies itself to Threads when requesting data. Threads will then validate your server by making a request to it, using the `facebookexternalua` user-agent found in your logs. If your server is blocked, no requests will be made.

## Threads must moderate users, not servers

It's very common on Mastodon to block servers with a heavy hand, with huge blocklists such as "Fediblock" naming nearly every server on the network. This is a big problem, because it creates a fragmented network where people cannot communicate with each other.

People have been wondering what Threads is going to do. Are they going to import a blocklist? Are they going to block individual servers at all?

Threads reports having 33 million daily-active users. The Fediverse has just a little over 1.2 million _monthly_ active users by some estimates. What is another million users to them? If they can afford to individually moderate 33 million users, they can afford to individually moderate 34 million users.

Therefore, I am a proponent that Threads should not block any servers at all, unless the server itself is behaving badly from a technical standpoint. They should moderate every user on an individual level, regardless of which server they're on, using the extensive moderation tools at their disposal. Remote users who violate the Threads Terms of Service should be banned, simple as that. Otherwise it is far too political, goes against the open nature of the Fediverse, and is just plain lazy on their part.

Despite everything, I think the people building Threads are decent, and I hope they do the right thing here.

## What now?

We will continue to monitor the situation. We will take actions to ensure the Fediverse stays **open and connected.**

Censorship on ActivityPub highlights the need for Nostr, a new protocol whose data portability empowers people to access content however they wish. The greatest fighting spirit is alive on Nostr, which we continue to build for a better future.

My upcoming project, [Ditto](/blog/soapbox-awarded-grant/), will enable people on Nostr to access the Fediverse from different domain names, making it more difficult to make sweeping blocks against Nostr in general. Follow my progress on it [here](https://gitlab.com/soapbox-pub/ditto).

And finally, I wish you all a very Merry Christmas! 🎄🎉
