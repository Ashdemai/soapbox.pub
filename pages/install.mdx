import Button from '../components/button';
import Layout from '../components/layout';
import Container from '../components/container';
import DateFormatter from '../components/date-formatter';
import Prose from '../components/prose';
import UnifiedMeta from '../components/unified-meta';
import Wrapper from '../components/wrapper';
import { getFileInfo } from '../lib/files';

# Installing Soapbox

Ready to run your own server? You can install it from scratch on a Linux machine, or pay for managed hosting.

- **Managed hosting** - you pay someone to install and manage the server. Great for novices and busy people.
- **Self-hosting** - you install and manage your own server. Preferred if you want total freedom and control.

## Option 1: Managed hosting by Btrf.ly

[Btrf.ly](https://btrf.ly/aff.php?aff=2) (pronounced _Butterfly_) provides **managed hosting** for Soapbox servers.
This is the easiest way to get started, as you can ignore pretty much the whole rest of this install guide and go through their checkout process instead.

<Button theme='secondary' href='https://btrf.ly/aff.php?aff=2'>Get started with Btrf.ly</Button>

_This is an affiliate link._

## Option 2: Installing on a VPS

We recommend installing Soapbox on a **dedicated VPS (virtual private server) running Ubuntu 22.04 LTS**. You should get your VPS up and running before starting this guide.

Some popular VPS hosting providers include:

- [DigitalOcean](https://m.do.co/c/84e2ff1e790f)<sup className='text-gray-600'>[affiliate link]</sup> — easy to use
- [Hetzner Cloud](https://www.hetzner.com/cloud) — cheap
- [BuyVM](https://buyvm.net/) — supports free speech

Expect to spend between **$10–15 USD/mo**, depending on the size of your community and how you choose to configure it.

You should already have a **domain name** from a registrar like [Namecheap](https://www.namecheap.com/). Create an `A` record with your registrar pointing to the IP address of your VPS.

![Terminal](/assets/images/terminal.png)

## 1. Shelling in

Once your VPS is running, you’ll need to open a **terminal program** on your computer. This will allow you to remotely connect to the server so you can run commands and install Soapbox.

Linux and Mac users should have a terminal program pre-installed (it’s just called **“Terminal”**), but Windows users may need to install [Cygwin](https://www.cygwin.com/) first.

Once the terminal is open, connect to your server with the username and IP address provided by your VPS host. It will likely prompt for a password.

```sh
ssh root@123.456.789
```

If you see a screen that looks like this, you’ve succeeded:

```
Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-65-generic x86_64)

  * Documentation:  https://help.ubuntu.com
  * Management:     https://landscape.canonical.com
  * Support:        https://ubuntu.com/advantage

  System information as of Wed Apr 28 18:59:27 UTC 2021

  System load:  1.86                Processes:              201
  Usage of /:   66.1% of 146.15GB   Users logged in:        0
  Memory usage: 29%                 IPv4 address for ens18: 10.0.0.100
  Swap usage:   4%                  IPv4 address for ens19: 192.168.1.100

  * Pure upstream Kubernetes 1.21, smallest, simplest cluster ops!

      https://microk8s.io/

79 updates can be installed immediately.
0 of these updates are security updates.
To see these additional updates run: apt list --upgradable


Last login: Tue Apr 27 17:28:56 2021 from 98.198.61.119
root@gleasonator:~#
```

## 2. System setup

Before installing Soapbox, we have to prepare the system.

### 2.a. Install updates

Usually a fresh VPS already has outdated software, so run the following commands to update it:

```sh
apt update
apt upgrade
```

When prompted (`[Y/n]`) type `Y` and hit Enter.

### 2.b. Install system dependencies

Soapbox relies on some additional system software in order to function. Install them with the following command:

```sh
apt install git curl build-essential postgresql postgresql-contrib cmake libmagic-dev imagemagick ffmpeg libimage-exiftool-perl nginx certbot unzip libssl-dev automake autoconf libncurses5-dev fasttext
```

### 2.c. Create the Pleroma user

For security reasons, it’s best to run Rebased as a separate user with limited access.

We’ll create this user and call it `pleroma`:

```sh
useradd -r -s /bin/false -m -d /var/lib/pleroma -U pleroma
```

## 3. Install Rebased

It’s time to install Rebased, the backend for Soapbox. Let’s get things up and running.

### 3.a. Downloading the source code

Download the Rebased source code with git:

```sh
git clone https://gitlab.com/soapbox-pub/rebased /opt/pleroma
chown -R pleroma:pleroma /opt/pleroma
```

Enter the source code directory, and become the pleroma user:

```sh
cd /opt/pleroma
sudo -Hu pleroma bash
```

(You should be the pleroma user in `/opt/pleroma` for the remainder of this section.)

### 3.b. Install Elixir

Rebased uses the Elixir programming language (based on Erlang). It’s important we use a specific version of Erlang (24), so we’ll use the [asdf version manager](https://asdf-vm.com/) to install it.

Install asdf:

```sh
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.0
echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc
echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc
exec bash
asdf plugin-add erlang
asdf plugin-add elixir
```

Finally, install Erlang/Elixir:

```sh
asdf install
```

(This will take about 15 minutes. ☕)

### 3.c. Compiling Rebased

Install basic Elixir tools for compilation:

```sh
mix local.hex --force
mix local.rebar --force
```

Fetch Elixir dependencies:

```sh
mix deps.get
```

Finally, compile Soapbox:

```sh
MIX_ENV=prod mix compile
```

(This will take about 10 minutes. ☕)

### 3.d. Generate the configuration

It’s time to preconfigure our instance. The following command will set up some basics such as your domain name:

```sh
MIX_ENV=prod mix pleroma.instance gen
```

If you’re happy with it, rename the generated file so it gets loaded at runtime:

```sh
mv config/generated_config.exs config/prod.secret.exs
```

### 3.e. Provision the database

The previous section also created a file called `config/setup_db.psql`, which you can use to create the database.

Exit back to the root user (for the remainder of this document):

```sh
exit
```

Execute the SQL file as the postgres user:

```sh
sudo -Hu postgres psql -f config/setup_db.psql
```

Now run the database migration as the pleroma user:

```sh
sudo -Hu pleroma bash -i -c 'MIX_ENV=prod mix ecto.migrate'
```

### 3.f. Start Rebased

Copy the systemd service and start Soapbox:

```sh
cp /opt/pleroma/installation/pleroma.service /etc/systemd/system/pleroma.service
systemctl enable --now pleroma.service
```

If you’ve made it this far, congrats! You’re very close to being done. Your Rebased server is running, and you just need to make it accessible to the outside world.

## 4. Getting online

The last step is to make your server accessible to the outside world. We’ll achieve that by installing Nginx and enabling HTTPS support.

### 4.a. HTTPS

We’ll use certbot to get an SSL certificate.

First, shut off Nginx:

```sh
systemctl stop nginx
```

Now you can get the certificate:

```sh
mkdir -p /var/lib/letsencrypt/
certbot certonly --email <your@emailaddress> -d <yourdomain> --standalone
```

Replace `<your@emailaddress>` and `<yourdomain>` with real values.

### 4.b. Nginx

Copy the example nginx configuration and activate it:

```sh
cp /opt/pleroma/installation/pleroma.nginx /etc/nginx/sites-available/pleroma.nginx
ln -s /etc/nginx/sites-available/pleroma.nginx /etc/nginx/sites-enabled/pleroma.nginx
```

You **must edit this file:**

```sh
nano /etc/nginx/sites-enabled/pleroma.nginx
```

Change all occurrences of `example.tld` with your site’s domain name. Use Ctrl+X, Y, Enter to save.

Finally, enable and start nginx:

```sh
systemctl enable --now nginx.service
```

🎉 Congrats, you’re done! Check your site in a browser and it should be online.

## 5. Install Soapbox

It’s finally time to install Soapbox itself! First, get the latest build.

```sh
curl -O https://dl.soapbox.pub/main/soapbox.zip
```

Next, unzip it.

```sh
busybox unzip soapbox.zip -o -d /opt/pleroma/instance/static
```

Refresh your website. That’s it!

## 6. Post-installation

Below are some additional steps you can take after you’ve finished installation.

### Create your first user

If your instance is up and running, you can create your first user with administrative rights with the following task:

```sh
cd /opt/pleroma
sudo -Hu pleroma bash -i -c 'MIX_ENV=prod mix pleroma.user new <username> <your@emailaddress> --admin'
```

### Renewing SSL

If you need to renew the certificate in the future, uncomment the relevant location block in the nginx config and run:

```sh
certbot certonly --email <your@emailaddress> -d <yourdomain> --webroot -w /var/lib/letsencrypt/
```

### Upgrading

To upgrade Rebased (the backend), shell into your server and issue the following commands.

```sh
sudo -Hu pleroma bash
cd /opt/pleroma

git pull origin main

asdf install

mix deps.get
MIX_ENV=prod mix ecto.migrate

exit
systemctl restart pleroma
```

To upgrade Soapbox (frontend), shell into your server and re-run the install commands.

```sh
curl -O https://dl.soapbox.pub/main/soapbox.zip

busybox unzip soapbox.zip -o -d /opt/pleroma/instance/static
```

## Questions

If you have questions or run into trouble, please [create an issue](https://gitlab.com/soapbox-pub/soapbox/-/issues) on the Soapbox GitLab.

export const getStaticProps = () => {
  return {
    props: {
      fileInfo: getFileInfo('pages/install.mdx'),
    },
  };
};

{/* https://www.codeconcisely.com/posts/nextjs-mdx/ */}
export default ({ children, fileInfo }) => (
  <Layout>
    <UnifiedMeta
      title='Install | Soapbox'
      description='Ready to install Soapbox? This guide will teach you how.'
      image='https://soapbox.pub/assets/images/terminal.png'
    />
    <Container>
      <Wrapper className='mb-36'>
        <Prose>
          {children}
        </Prose>

        <p className='text-gray-500 mt-12'>
          Last edited <a className='text-azure' href={fileInfo.sourceUrl} target='_blank'><DateFormatter dateString={fileInfo.lastModified} /></a>.
        </p>
      </Wrapper>
    </Container>
  </Layout>
);