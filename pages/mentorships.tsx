import Container from '../components/container';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import UnifiedMeta from '../components/unified-meta';
import Wrapper from '../components/wrapper';


export default function MentorshipsPage() {
  return (
    <Layout>
      <UnifiedMeta
        title='Mentorships | Soapbox'
      />
      <Container>
        <Wrapper>

          <PageTitle className='my-16'>
            Developer Mentorships
          </PageTitle>

          <div className='space-y-6 pb-36'>
            <h2 className='text-2xl font-semibold leading-snug'>
              Soapbox mentorships for new developers
            </h2>
            <p>
              At Soapbox, we care about growing the free software community. That's why we provide mentorships to new developers who are interested in being contributors to Sopabox's open source projects, with a focus on individuals in developing countries who may not have access to other opportunities.
            </p>

            <h3 className='text-xl font-semibold leading-snug'>
              Mentorships include:
            </h3>
            <ul className='list-disc pl-8'>
              <li>
                Monthly stipend paid in Bitcoin
              </li>
              <li>
                1:1 mentorship and code review
              </li>
              <li>
                Become part of a passionate global team of developers
              </li>
              <li>
                Get credit for your work, and grow your portfolio
              </li>
            </ul>

            <p className='text-sm'>To apply, open an issue on <a className='text-azure underline' href='https://gitlab.com/soapbox-pub/soapbox/-/issues' target='_blank'>GitLab</a> and describe how and why you want to get involved!</p>

            <div className='relative flex py-8 items-center'>
              <div className='flex-grow border-t border-gray-200'></div>
            </div>
            
            <h2 className='text-2xl font-semibold'>
              Current Mentorship Openings
            </h2>
            
            <div className='space-y-1'>
              <h3 className='text-xl font-semibold'>
                Web Designer for Ditto
              </h3>
              <p>Soapbox is seeking a designer to work on the UI and UX of <a className='text-azure underline' href='/ditto' target='_blank'>Ditto</a> a new community-based client for Nostr. The ideal candidate is a recent graduate or student with some experience, looking to grow their portfolio.</p>
            </div>

            <div className='space-y-1'>
              <h3 className='text-xl font-semibold'>
                Full Stack Contributors
              </h3>
              <p><a className='text-azure underline' href='/ditto' target='_blank'>Soapbox</a> is always looking for passionate contributors at any level of our stack. Some tech we use include: React, Typescript, Deno, Tailwind, and Elixir. Got skills and passion? Get in touch!</p>
            </div>

            <p className='text-sm'>Don't see what you're looking to work on? Browse <a className='text-azure underline' href='https://gitlab.com/soapbox-pub/soapbox/-/issues' target='_blank'>open issues on GitLab</a> to begin contributing today and get involved!</p>
            
          </div>
        
        </Wrapper>

      </Container>
    </Layout>
  );
}
