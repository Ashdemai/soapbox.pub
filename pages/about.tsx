import { IconArrowRight } from '@tabler/icons';
import Link from 'next/link';

import Button from '../components/button';
import Container from '../components/container';
import ContributorAvatars from '../components/contributor-avatars';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import TeamMember from '../components/team-member';
import UnifiedMeta from '../components/unified-meta';
import Wrapper from '../components/wrapper';
import { getTopContributors } from '../lib/contributors';

import type { Types as GitlabTypes } from '@gitbeaker/node';

interface IAboutPage {
  contributors: GitlabTypes.UserSchema[]
}

export default function AboutPage({ contributors }: IAboutPage) {
  return (
    <Layout>
      <UnifiedMeta
        title='About | Soapbox'
      />
      <Container>

       <div className='w-3/4 flex flex-col justify-center mx-auto'>

          <PageTitle className='my-16'>
            Meet the Team
          </PageTitle>

          <div className='grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-20'>
            <TeamMember
              name='Alex Gleason'
              avatar='/assets/avatars/alex.png'
              bio='Alex is the Founder and CEO of Soapbox. He has over a decade of experience building social platforms, and is one of the foremost experts on ActivityPub and the Fediverse. He is still learning, and still dreaming.'
              url='https://njump.me/nprofile1qqs8nsk2uy2w529fs8n4tx60u7z553e4yx5dy2nxhw4elgjgawpqlaspz4mhxue69uhhyetvv9ujumt0wd68ytnsw43q4hag5n'
            />

            <TeamMember
              name='Patrick dos Reis'
              avatar='/assets/avatars/patrick.png'
              bio='Patrick is interested in working in disruptive technologies like Ditto that can be used to empower individual freedom. He also loves nature.              '
              url='https://njump.me/npub1gujeqakgt7fyp6zjggxhyy7ft623qtcaay5lkc8n8gkry4cvnrzqd3f67z'
            />

            <TeamMember
              name='shantaram'
              avatar='/assets/avatars/shantaram.jpeg'
              bio='Self-taught software engineer writing software to achieve a net good effect on the world.'
              url='https://njump.me/npub1q469xmf77nt9ltu4ks3excgts36ayt6v99ryn4rv2r2axmm4ye7q3vnkqp'
            />

            <TeamMember
              name='M. K. Fain'
              avatar='/assets/avatars/mk.jpg'
              bio='M. K. is a marketing and communications consultant with a focus on nonprofits and technology. She is passionate about freedom and building spaces for women online.'
              url='https://njump.me/npub1jvnpg4c6ljadf5t6ry0w9q0rnm4mksde87kglkrc993z46c39axsgq89sc'
            />

          </div>
        </div>

        <Wrapper>
          <h2 className='text-2xl font-semibold mt-24 mb-8 text-center leading-snug'>
            Thanks to the Community!
          </h2>

          <div className='flex justify-center'>
            <ContributorAvatars
              contributors={contributors}
              size='lg'
            />
          </div>

          <div className='space-y-6'>
            <h3 className='text-xl font-semibold mt-20 leading-snug'>
              Want to get involved?
            </h3>
            <p>Soapbox is seeking active contributors to join our volunteer and freelance community. Browse <a className='text-azure underline' href='https://gitlab.com/soapbox-pub/soapbox/-/issues' target='_blank'>open issues on GitLab</a> to begin contributing today!</p>
            <p>We also offer paid mentorships for young or new developers who are passionate about free software. <a className='text-azure underline' href='/mentorships'>Learn more.</a></p> 
          </div>
          </Wrapper>

        <div className='flex flex-col lg:flex-row gap-20 my-20 lg:my-32'>
          <AboutCta
            title='Federated Social Media… Without the barriers.'
            description='Soapbox is a powerful frontend for the most popular federated social networks. Currently home to some of the largest servers on the Fediverse, Soapbox makes the freedom of decentralized communication accessible to all by focusing on a familiar and friendly user interface with all the features today’s users have come to expect.'
            image='/assets/blog/take-down-big-tech-with-great-ux/great-ui-2-1.png'
            url='/blog/take-down-big-tech-with-great-ux/'
          />

          <AboutCta
            title='Built for sustainable growth'
            description='Soapbox focuses on user experience, discoverability, and providing monetization avenues for server hosts and content creators.  Leading the way in the Fediverse on important growth features like quote posting, donations, and crypto integration, Soapbox is the front-end choice for platforms that want to attract users and keep them coming back for more.'
            image='/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/v-1-3-thumb.png'
            url='/blog/soapbox-fe-v1.3-cryptocurrency-release/'
          />

          <AboutCta
            title='Always Open Source, Always Free'
            description='Soapbox is dedicated to the mission and power of open source software. Soapbox is licensed under the AGPL 3.0 and is always free to use, run, copy, and build on. We are proudly providing jobs for free software contributors across the globe who share our vision! Soapbox is spreading the freedom of decentralized social media a mainstream audience.'
            image='/assets/blog/future-is-decentralized/decentralized-future-1.png'
            url='/blog/future-is-decentralized/'
          />
        </div>
      </Container>
    </Layout>
  );
}

interface IAboutCta {
  title: string
  description: string
  image?: string
  url?: string
}

const AboutCta: React.FC<IAboutCta> = ({ title, description, image, url }) => {
  return (
    <div className='flex gap-6'>
      <div className='space-y-5'>
        {image && (
          <Link href={url}>
            <img
              className='rounded-xl object-cover object-center'
              src={image}
            />
          </Link>
        )}
        <h2 className='text-2xl font-semibold text-azure'>
          <Link href={url}>{title}</Link>
        </h2>
        <p>{description}</p>
        {url && (
          <Button theme='secondary' href={url} group>
            <span className='flex items-center space-x-2 group'>
              <span>Learn more</span>
              <IconArrowRight className='transition-all w-5 group-hover:translate-x-1' />
            </span>
          </Button>
        )}
      </div>
    </div>
  );
};

export const getStaticProps = async () => {
  const contributors = await getTopContributors(6, 6000, {
    exclude: [737172, 20692576, 20626644], // skip hardcoded team members
  });

  return {
    props: {
      contributors,
    },
  };
};
