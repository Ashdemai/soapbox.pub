import { Gitlab, Types as GitlabTypes } from '@gitbeaker/node';

import Container from '../../components/container';
import Layout from '../../components/layout';
import Release from '../../components/release';
import UnifiedMeta from '../../components/unified-meta';
import Wrapper from '../../components/wrapper';

interface Props {
  release: GitlabTypes.ReleaseSchema
}

export default function ReleasePage({ release }: Props) {
  return (
    <Layout>
      <UnifiedMeta
        title={`Soapbox ${release.tag_name}`}
        description={`Things we added, changed, fixed and removed in Soapbox ${release.tag_name}!`}
      />
      <Container>
        <Wrapper className='my-12 mb-36'>
          <Release
            key={release.tag_name}
            release={release}
          />
        </Wrapper>
      </Container>
    </Layout>
  );
}

type Params = {
  params: {
    slug: string
  }
}

export const getStaticProps = async ({ params }: Params) => {
  const api = new Gitlab({
    host: 'https://gitlab.com',
  });

  const release = await api.Releases.show(
    '17765635',
    params.slug,
    {
      // @ts-ignore
      include_html_description: true,
    },
  );

  return {
    props: { release },
  };
};

export async function getStaticPaths() {
  const api = new Gitlab({
    host: 'https://gitlab.com',
  });

  const releases = await api.Releases.all('17765635');
  const paths = releases.map((release) => `/releases/${release.tag_name}`);

  return {
    paths,
    fallback: false,
  };
}