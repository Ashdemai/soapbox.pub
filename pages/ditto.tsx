import Banner from '../components/banner';
import Button from '../components/button';
import Container from '../components/container';
import FeatureSection from '../components/feature-section';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import TeamMember from '../components/team-member';
import UnifiedMeta from '../components/unified-meta';


export default function DittoPage() {
  return (
    <Layout>
      <UnifiedMeta title='Ditto | Soapbox' />

      <Banner 
          heading='Ditto.'
          subheadings={[
            'Build Your Community on Nostr',
          ]}
          paragraph='Ditto is a decentralized, self-hosted social media server that emphasizes user control and community building across platforms. Key features include a built-in Nostr relay, compatibility with any Mastodon app, and full customizability. Always open-source, no ads, no tracking, and no censorship. '
          mobileFadeClassName='from-violet-800 via-violet-800'
          backgroundColorClassName='bg-purple'
          imageUrl='/assets/images/ditto/ditto-banner.png'
          mobileImageUrl='/assets/images/ditto/ditto-banner.png'
        />

      <Container className='my-24'>

        <FeatureSection 
          title='Made To Fit Your Unique Style'
          subtitle='Host your own server with customized themes and community moderation' 
          description='Ditto offers robust moderation tools at both the server and user level so that both communities and the individuals within them can craft their ideal experience on Nostr.' 
          images={[
              '/assets/images/ditto/theme-blue.png',
              '/assets/images/ditto/theme-dark-blue.png',
              '/assets/images/ditto/theme-pink.png',
              '/assets/images/ditto/theme-dark-purple.png',
              '/assets/images/ditto/theme-orange.png',
              '/assets/images/ditto/theme-dark-orange.png',
              '/assets/images/ditto/theme-green.png',
              '/assets/images/ditto/theme-dark-green.png',
              '/assets/images/ditto/theme-red.png',
              '/assets/images/ditto/theme-dark-red.png',
          ]}
          orientation='left'
          button={true}
          buttonText='Learn About Our Creator Program'
          buttonLink='/creators'
          buttonFull={true}
        />

        <FeatureSection 
          title='Fully Customizable Themes'
          subtitle='Brand and Monetize Your Server' 
          description="Emphasize your server's unique flair by adding custom themes and logos. Ditto puts you in charge, encouraging the creation of a name and logo that truly resonate with your community. Define your entire color scheme through a single hex code, applicable seamlessly across both light and dark themes."
          image='/assets/images/ditto/theme-editor.png'
          orientation='right'
        />
        <FeatureSection 
          title='Identity-Based Local Feeds'
          subtitle='Use NIP-05 identities to curate the community timeline' 
          description='NIP-05 identities (usernames granted by a specific domain) mark users as part of your community. Ditto features a built-in self-service name provider for users, which admins can approve or revoke. This is how Ditto admins will determine who is part of their community, and how users will indicate their choice of home server.'
          image='/assets/blog/curated-communties/local-feed.png'
          orientation='left'
        />
        <FeatureSection 
          title='Connect Across Mulitple Decentralized Platforms'
          subtitle='Bringing Nostr and the Fediverse together' 
          description='Connect with the global community of decentralized users thanks to integration with the Mostr bridge. Follow and interact with users from both Nostr and Fediverse.' 
          image='/assets/images/ditto/global-phone.gif'
          orientation='right'
          shadow={false}
          button={true}
          buttonText='Try it out on Gleasonator.dev'
          buttonLink='https://gleasonator.dev'
          buttonFull={false}
        />
        <FeatureSection 
          title='Zap Lightning Payments'
          subtitle='Monetize your content with micro transactions supporter by the Bitcoin Lightning network' 
          description="Clicking the 'Zap' button on a post will send a small amount of Bitcoin (aka satoshis or 'sats') to the author of the post. It means anyone in the world, including you, can get paid just for posting online." 
          image='/assets/images/ditto/lightning.gif'
          orientation='left'
        />
      </Container>

      <Container className='flex justify-center my-48'>
        <div className='flex flex-col items-center space-y-8 text-center rounded-full bg-white shadow-white shadow-[0_20px_50px_50px]'>
          <h2 className='text-3xl lg:text-5xl font-semibold leading-none'>
            Related Projects
          </h2>
          <h2 className='text-xl font-semibold mt-6 mb-24'>
            Building an flexible ecosystem with tools for use across platforms
          </h2>

          <div className='grid lg:grid-cols-3 gap-20'>
            <TeamMember
              name='Mostr Bridge'
              avatar='/assets/images/ditto-cartoon-planet.png'
              bio='Mostr is a bridge between Nostr and the Fediverse (Mastodon, ActivityPub, etc). It allows users on both networks to communicate, through a Mostr server.'
              url='https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/'
            />

            <TeamMember
              name='Nostrify'
              avatar='/assets/images/ditto-cartoon-ufo.png'
              bio='Bring your projects to life on Nostr with this flexible and pure Typscript framework on Deno and web.'
              url='https://nostrify.dev/'
            />

            <TeamMember
              name='Strfry Policies'
              avatar='/assets/images/ditto-cartoon-telescope.png'
              bio='A collection of policies for the strfry Nostr relay, built in Deno.'
              url='https://gitlab.com/soapbox-pub/strfry-policies'
            />
          </div>
        </div>
      </Container>

      <div className='bg-gradient-to-r from-indigo-500 to-blue-500 text-white py-20'>
        <Container>
          <div className='max-w-prose space-y-6'>
            <h2 className='text-4xl font-semibold'>Get Involved</h2>
            <p className='text-xl'>Soapbox is seeking active contributors to join our contributor community. Browse open issues on GitLab to begin contributing today!</p>
            <div className='space-x-4'>
              <Button theme='secondary' href='https://gitlab.com/soapbox-pub/ditto'>Contribute</Button>
            </div>
            <p>Looking for work? We're also looking for core contributors open to contract. <br />Get in touch if that could be you!</p>
          </div>
        </Container>
      </div>

    </Layout>
  );
}