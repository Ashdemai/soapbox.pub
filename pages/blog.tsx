import { allPosts, Post } from 'contentlayer/generated';
import { compareDesc } from 'date-fns';

import HeroPost from '../components/blog/hero-post';
import MoreStories from '../components/blog/more-stories';
import Container from '../components/container';
import Layout from '../components/layout';
import UnifiedMeta from '../components/unified-meta';
import { generateRssFeed } from '../lib/rss';

type Props = {
  posts: Post[]
}

export default function Index({ posts }: Props) {
  const heroPost = posts[0];
  const morePosts = posts.slice(1);
  return (
    <Layout>
      <UnifiedMeta
        title='Blog | Soapbox'
      />
      <Container>
        {heroPost && (
          <div className='my-12'>
            <HeroPost
              title={heroPost.title}
              coverImage={heroPost.coverImage}
              date={heroPost.date}
              author={heroPost.author}
              slug={heroPost.slug}
              excerpt={heroPost.excerpt}
            />
          </div>
        )}
        {morePosts.length > 0 && (
          <MoreStories posts={morePosts} />
        )}
      </Container>
    </Layout>
  );
}

export const getStaticProps = async () => {
  await generateRssFeed();

  const posts = allPosts.sort((a, b) => {
    return compareDesc(new Date(a.date), new Date(b.date));
  }).map(post => {
    // For performance, drop the post body from the index.
    return { ...post, body: null };
  });

  return {
    props: { posts },
  };
};
