import clsx from 'clsx';
import Image from 'next/image';

import Button from '../components/button';
import CommitGraph from '../components/commit-graph';
import Container from '../components/container';
import ContributorAvatars from '../components/contributor-avatars';
import DateFormatter from '../components/date-formatter';
import { Features, Feature } from '../components/feature';
import Layout from '../components/layout';
import TiltDevices from '../components/tilt-devices';
import { getCommitFrequency, getTopContributors } from '../lib/contributors';

import type { Types as GitlabTypes } from '@gitbeaker/node';

interface IHomepage {
  contributors: GitlabTypes.UserSchema[]
  commitFrequency: number[]
  buildTime: string
}

export default function Homepage({ contributors, commitFrequency, buildTime }: IHomepage) {
  return (
    <Layout>
      <Container>
        <h1 className='my-16 sm:my-24 md:my-36 text-3xl sm:text-6xl md:text-7xl text-center font-extrabold leading-tight md:leading-none'>
          <span>Powering the future</span>
          <br />
          <span className='text-azure'>of social media</span>.
        </h1>

        <div className='pt-16 -mt-16 sm:pt-24 sm:-pt-24 md:pt-36 md:-mt-36 overflow-hidden'>
          <TiltDevices />
        </div>

        <div className='my-8'>
          <Features>
            <Feature
              title={<>Tell us how you <span className='font-bold whitespace-nowrap'>really feel</span>.</>}
              blurb='Emoji reactions allow you to express yourself with more than just a “like.”'
            >
              <video
                className='max-h-[200px]'
                src='/assets/features/emoji-reaction.webm'
                autoPlay
                muted
                loop
              />
            </Feature>

            <Feature
              span={2}
              title={<>Broaden&nbsp;your <span className='font-bold'>audience</span>.</>}
              blurb='Quote posting lets you communicate effectively by providing context for your posts.'
              direction='horizontal'
            >
              <div className='relative'>
                <Image
                  className='h-full object-cover object-left-top'
                  src='/assets/features/quote-post.png'
                  width='1100'
                  height='620'
                  alt='Quote post'
                  unoptimized
                />

                <div className='absolute h-full w-full z-10 inset-y-0 right-0 bg-gradient-to-l from-white' />
              </div>
            </Feature>

            <Feature
              span={2}
              title={<>Save <span className='font-bold'>the&nbsp;date</span>.</>}
              blurb='Schedule events to coordinate Hackathons, meetups, and more!'
              direction='horizontal'
            >
              <div className='relative'>
                <Image
                  className='h-full object-cover object-left-top'
                  src='/assets/features/events.png'
                  width='1150'
                  height='750'
                  alt='Quote post'
                  unoptimized
                />

                <div className='absolute w-full h-1/3 z-10 inset-x-0 bottom-0 bg-gradient-to-t from-white' />
              </div>
            </Feature>

            <Feature
              span={1}
              className='min-h-[400px] text-white'
              title={<>Reach across <span className='font-bold'>the&nbsp;Fediverse</span>.</>}
              blurb='Soapbox is built on the ActivityPub protocol, so you can follow people on Mastodon, Pleroma, and more.'
            >
              <Image
                className='absolute inset-0 w-full h-full object-cover -z-20'
                src='/assets/images/space.jpg'
                width='1280'
                height='800'
                alt='Fediverse'
                unoptimized
              />

              <div className='absolute h-full w-full -z-10 inset-0 bg-gradient-to-b from-black' />
            </Feature>
          </Features>
        </div>
      </Container>

      <div className="h-full w-full bg-[url('/assets/images/stary-background.png')]">
        <div className='bg-gradient-to-b from-white min-h-[800px] h-full w-full'>
          <div className='bg-gradient-to-t from-white min-h-[800px] h-full w-full'>
            <div className='flex flex-col lg:flex-row items-stretch justify-items-stretch gap-16 py-24 container mx-auto'>
              <img className='rounded-2xl lg:max-w-prose shadow' src='/assets/images/ditto-screenshot.png' />

              <Feature
                span={2}
                title={<>Coming Soon: <span className='font-bold whitespace-nowrap'>Ditto</span>.</>}
                blurb='Ditto is a decentralized, self-hosted social media server that emphasizes user control and community building across platforms. Key features include a built-in Nostr relay, compatibility with any Mastodon app, and full customizability. Always open-source, no ads, no tracking, and no censorship.'
              >
                <div className='px-6 pb-6'>
                  <Button theme='secondary' href='/ditto/'>Learn More</Button>
                </div>
              </Feature>
            </div>
          </div>
        </div>
      </div>

      <div className='-mt-24 pt-16 pb-36 lg:pt-36 lg:pb-48 relative'>
        <div className='w-full h-full overflow-hidden'>
          <div className={clsx(
            'absolute w-full bottom-0 right-0 md:inset-x-0',
            'h-1/3 lg:h-1/2 2xl:h-2/3',
            'origin-right scale-x-[4] md:scale-x-[2] lg:scale-x-150 2xl:scale-100',
            'opacity-40 pointer-events-none -z-10',
          )}>
            <CommitGraph data={[...commitFrequency].reverse()} />
            <div className='absolute w-full h-[30px] z-10 inset-x-0 bottom-0 bg-gradient-to-t from-white' />
          </div>
        </div>

        <Container className='flex justify-center'>
          <div className='flex flex-col items-center space-y-8 text-center rounded-full bg-white shadow-white shadow-[0_20px_50px_50px]'>
            <h2 className='text-3xl md:text-5xl lg:text-6xl font-semibold leading-none'>
              A vibrant open source community
            </h2>
  
            <p className='text-xl lg:text-2xl max-w-lg'>
              Soapbox is built by people around the world to empower communities online.
            </p>

            <ContributorAvatars contributors={contributors} />

            <Button href='https://gitlab.com/groups/soapbox-pub/-/activity' theme='secondary'>
              View Activity
            </Button>

            <p className='text-gray-500 text-sm'>
              Data pulled from GitLab API <DateFormatter dateString={buildTime} format='LLL d, yyyy' />.
            </p>
          </div>
        </Container>
      </div>

      <div className='bg-azure text-white py-20'>
        <Container>
          <div className='max-w-prose space-y-6'>
            <h2 className='text-4xl font-semibold'>Ready to get started?</h2>
            <p className='text-xl'>Install your own server to have complete control over your experience, or try Soapbox against any server.</p>
            <div className='space-x-4'>
              <Button theme='secondary' href='https://fe.soapbox.pub'>Try it</Button>
              <Button theme='secondary' href='/install'>Install</Button>
            </div>
          </div>
        </Container>
      </div>
    </Layout>
  );
}

export const getStaticProps = async () => {
  const contributors = await getTopContributors(4, 300);
  const commitFrequency = await getCommitFrequency(90);

  return {
    props: {
      contributors,
      commitFrequency,
      buildTime: new Date().toISOString(),
    },
  };
};
