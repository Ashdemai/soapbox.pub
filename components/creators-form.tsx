'use client';
 
import { useForm, ValidationError } from '@formspree/react';

import Button from './button';

export default function CreatorsForm() { 

  const [state, handleSubmit] = useForm('xoqgbgzw');

  const formTitle = 'Apply';
  const formSubtitle = 'Launch your platform and build your community';

  if (state.succeeded) {
    
    return (
      <div className='flex w-full justify-center'>
        <div className='flex flex-col items-left space-y-8 max-w-prose mt-20 justify-self-center place-self-center'>
          <h1 className='text-azure text-6xl lg:text-7xl font-bold'>
            {formTitle}
          </h1>
          <h2 className='text-2xl md:text-3xl font-semibold mb-8'>
            {formSubtitle}
          </h2>
          <p className='my-20 font-semibold text-azure text-xl text-left'>Thanks for your submission! We'll be in touch with your shortly.</p>
        </div>
      </div>
     );


  }

  return (
    <form onSubmit={handleSubmit}>

        <div className='flex w-full justify-center'>

        <div className='flex flex-col items-left space-y-8 max-w-prose mt-20 justify-self-center place-self-center'>

            <h1 className='text-azure text-6xl lg:text-7xl font-bold'>
              {formTitle}
            </h1>
            <h2 className='text-2xl md:text-3xl font-semibold mb-8'>
              {formSubtitle}
            </h2>

            <div className='flex flex-col items-left'>
                <label htmlFor='name'>Name</label>
                <input className='text-slate-700 border border-slate-400 rounded-lg p-2' 
                type='text' name='name' id='name' placeholder='Your Name' required={true} />
                <ValidationError prefix='Name' field='name' errors={state.errors} />
            </div>
            
            <div className='flex flex-col items-left'>
                <label htmlFor='email-address'>Email Address</label>
                <input className='text-slate-700 border border-slate-400 rounded-lg p-2' 
                type='email' name='_replyto' id='email-address' placeholder='email@domain.tld' required={true} />
                <ValidationError prefix='Email' field='email' errors={state.errors} />
            </div>

            <div className='flex flex-col items-left'>
                <label htmlFor='message'>Please tell us about your exsiting social media or content platforms</label>
                <textarea className='text-slate-700 border border-slate-400 rounded-lg p-2' 
                rows={5} name='social' id='social' required={true} placeholder='Do you already have a following? Have you built a a subscription list? Drop your links!'></textarea>
                <ValidationError prefix='Social' field='social' errors={state.errors} />
            </div>

            <div className='flex flex-col items-left'>
                <label htmlFor='message'>Why do you want to join the the Ditto Creator Incubator?</label>
                <textarea className='text-slate-700 border border-slate-400 rounded-lg p-2' 
                rows={5} name='message' id='message' placeholder='Tell us about what brings you to Nostr or how you heard about us' required={true}></textarea>
                <ValidationError prefix='Message' field='message' errors={state.errors} />
            </div>
        
        <input type='hidden' name='_subject' id='email-subject' value='Contact Form Submission' />

          <button
          className='text-lg py-4 px-8 rounded-full font-semibold text-center text-white
          bg-gradient-to-r from-sky-500 to-violet-500 hover:bg-gradient-to-r hover:from-sky-500 hover:to-violet-600' 
          type='submit' disabled={state.submitting}>Apply</button>

        <ValidationError errors={state.errors} />

        </div>

        </div>

    </form>
  );
}