import React from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines';

interface ICommitGraph {
  data: ReadonlyArray<number>
}

/** Display a line graph for commit history. */
const CommitGraph: React.FC<ICommitGraph> = ({ data }) => {
  return (
    <Sparklines
      data={[...data].reverse()}
      width={1000}
      height={250}
      style={{ width: '100%', height: '100%' }}
    >
      <SparklinesLine color='#0482d8' />
    </Sparklines>
  );
};

export default CommitGraph;