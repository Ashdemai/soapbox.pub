import { IconDownload } from '@tabler/icons';
import Link from 'next/link';

import DateFormatter from './date-formatter';
import Prose from './prose';

import type { Types as GitlabTypes } from '@gitbeaker/node';

interface IRelease {
  release: GitlabTypes.ReleaseSchema
  tada?: boolean
}

const Release: React.FC<IRelease> = ({ release, tada }) => {
  const downloadUrl = `https://gitlab.com/soapbox-pub/soapbox/-/jobs/artifacts/${release.tag_name}/download?job=build-production`;

  return (
    <div className='space-y-8'>
      <div className='space-y-2'>
        <h2 className='text-3xl'>
          <Link href={`/releases/${release.tag_name}`}>
            Soapbox {release.name} {tada ? '🎉' : ''}
          </Link>
        </h2>

        <div className='text-gray-500'>
          <DateFormatter dateString={release.released_at} />
        </div>
      </div>

      <a
        className='flex space-x-2 text-azure'
        href={downloadUrl}
        target='_blank'
      >
        <IconDownload />
        <span>Download</span>
      </a>

      <Prose dangerouslySetInnerHTML={{ __html: release.description_html }} />
    </div>
  );
};

export default Release;