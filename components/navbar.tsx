import clsx from 'clsx';
import throttle from 'lodash/throttle';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';

import Container from './container';
import Logo from './logo';
import MenuButton from './menu-button';

const Navbar: React.FC = () => {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => setMenuOpen(!menuOpen);
  const closeMenu = () => setMenuOpen(false);

  const handleResize = useCallback(throttle(() => {
    if (window.innerWidth >= 1024) {
      closeMenu();
    }
  }, 50), []);

  useEffect(() => {
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    document.documentElement.classList.toggle('overflow-hidden', menuOpen);
  }, [menuOpen]);

  return (
    <nav className={clsx('flex flex-col z-50 sticky top-0 backdrop-blur overflow-hidden transition-all', {
      'bg-white/60': !menuOpen,
      'bg-white/95 backdrop-blur-md h-screen': menuOpen,
    })}
    >
      <Container>
        <div className='py-4 lg:py-8 space-x-10 flex justify-between items-center'>
          <Link href='/' className='relative flex justify-between'>
            <Logo withText />
          </Link>

          <div>
            <div className='hidden lg:flex items-center space-x-10'>
              <NavbarLink href='/about'>
                About
              </NavbarLink>

              <NavbarLink href='/ditto'>
                Ditto
              </NavbarLink>

              <NavbarLink href='/servers'>
                Servers
              </NavbarLink>

              <NavbarLink href='/install'>
                Install
              </NavbarLink>

              <NavbarLink href='/blog'>
                Blog
              </NavbarLink>

              <NavbarLink href='/donate'>
                Donate
              </NavbarLink>
            </div>

            <div className='lg:hidden text-gray-600'>
              <MenuButton
                active={menuOpen}
                onClick={toggleMenu}
              />
            </div>
          </div>
        </div>
      </Container>

      <div
        className={clsx('overflow-y-auto', {
          'hidden': !menuOpen,
        })}
      >
        <Container className='my-10 space-y-10'>
          <MobileLink href='/about' onClickCurrent={closeMenu}>
            About
          </MobileLink>

          <MobileLink href='/ditto' onClickCurrent={closeMenu}>
            Ditto
          </MobileLink>

          <MobileLink href='/blog' onClickCurrent={closeMenu}>
            Blog
          </MobileLink>

          <MobileLink href='/servers' onClickCurrent={closeMenu}>
            Servers
          </MobileLink>

          <MobileLink href='/install' onClickCurrent={closeMenu}>
            Install
          </MobileLink>

          <MobileLink href='/donate' onClickCurrent={closeMenu}>
            Donate
          </MobileLink>
        </Container>
      </div>
    </nav>
  );
};

interface INavbarLink {
  href: string
  children: React.ReactNode
}

const NavbarLink: React.FC<INavbarLink> = ({ href, children }) => {
  return (
    <Link href={href} className='block font-semibold text-sm md:text-base text-gray-600'>
      {children}
    </Link>
  );
};

interface IMobileLink {
  href: string
  children: React.ReactNode
  /** Callback when a link to the current page is clicked. */
  onClickCurrent?: React.MouseEventHandler<HTMLAnchorElement>
}

const MobileLink: React.FC<IMobileLink> = ({ href, children, onClickCurrent }) => {
  const { pathname } = useRouter();

  const handleClick: React.MouseEventHandler<HTMLAnchorElement> = (e) => {
    if (onClickCurrent && href.replace(/\/$/, '') === pathname) {
      onClickCurrent(e);
    }
  };

  return (
    <Link
      href={href}
      onClick={handleClick}
      className='block font-semibold text-2xl text-gray-600'
    >
      {children}
    </Link>
  );
};

export default Navbar;