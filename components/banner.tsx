import clsx from 'clsx';
import React, { useState, useEffect } from 'react';

interface BannerProps {
  heading: string;
  subheadings: string[];
  paragraph?: string;
  imageUrl: string;
  mobileImageUrl?: string;
  mobileFadeClassName?: string;
  backgroundColorClassName?: string;
}

const Banner: React.FC<BannerProps> = ({ heading, subheadings, paragraph, imageUrl, mobileImageUrl, mobileFadeClassName, backgroundColorClassName }) => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 800); 
    };

    handleResize();

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className='space-y-24'>
      {isMobile && mobileImageUrl ? (
        <div
          className='inset-0 bg-left bg-cover'
          style={{ backgroundImage: `url(${mobileImageUrl})` }}
        >
          <div className={clsx('bg-gradient-to-r  h-100 flex flex-col items-start p-16 space-y-4 lg:space-y-8 lg:w-1/2', mobileFadeClassName)}>
            <div className='md:mt-12'>
              <h1 className='text-white text-6xl lg:text-7xl xl:text-8xl 2xl:text-9xl font-bold'>{heading}</h1>
            </div>
            {subheadings.map((subheading, index) => (
              <h2 key={index} className='text-white md:text-xl lg:text-3xl 2xl:text-4xl font-semibold'>{subheading}</h2>
            ))}
            {paragraph && <p className='text-xl text-white'>{paragraph}</p>}
          </div>
        </div>
      ) : (
        <div className={clsx('md:h-1/2 lg:h-3/4 w-full', backgroundColorClassName)}>
          <div className='relative h-full md:pl-8'>
            <div className={clsx('py-20 flex flex-col items-start p-16 space-y-4 lg:space-y-8 md:w-1/2 md:pb-40', backgroundColorClassName)}>
              <div className='md:mt-12'>
                <h1 className='text-white text-6xl lg:text-7xl xl:text-8xl 2xl:text-9xl font-bold leading-none mb-10'>{heading}</h1>
              </div>
              {subheadings.map((subheading, index) => (
                <h2 key={index} className='text-white md:text-xl lg:text-3xl 2xl:text-4xl font-semibold'>{subheading}</h2>
              ))}
              {paragraph && <p className='text-white text-xl'>{paragraph}</p>}
            </div>
            <div
              className='md:ml-20 lg:ml-40 clip-path-diagonal-right absolute inset-0 bg-center xl:bg-right bg-cover 2xl:bg-contain'
              style={{ backgroundImage: `url(${imageUrl})` }}
            ></div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Banner;
