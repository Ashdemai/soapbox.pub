import React from 'react';
import Slider from 'react-slick';
import { useSpring, animated } from 'react-spring';

import Button from '../components/button';


import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

interface FeatureSectionProps{
  title: string;
  subtitle: string;
  description: string;
  image?: string;
  images?: string[];
  orientation: string;
  bgColor?: string;
  bgImage?: string;
  shadow?: boolean;
  button?: boolean;
  buttonText?: string;
  buttonLink?: string;
  buttonFull?: boolean;
}

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  adaptiveHeight: true,
  autoplay: true,
  autoplaySpeed: 3500,
  arrows: false,
  loops: true,
  rtl: true,
};

const FeatureSection: React.FC<FeatureSectionProps> = ({ title, subtitle, description, image, images, button, buttonLink, buttonText, buttonFull, orientation, bgColor, bgImage, shadow = true }) => {
  const props = useSpring({ opacity: 1, from: { opacity: 0 } });

  const sectionClasses = orientation === 'right' 
    ? 'flex flex-col-reverse lg:flex-row-reverse lg:items-center justify-around gap-y-8 my-16' 
    : 'flex flex-col-reverse lg:flex-row lg:items-center justify-around gap-y-8 my-16';

  const sectionStyle = {
    backgroundImage: bgImage ? `url(${bgImage})` : 'none',
    backgroundColor: bgColor,
  };

  const imageClasses = shadow ? 'w-fit max-h-[650px] rounded-2xl shadow object-cover transform transition-transform hover:scale-110' 
                             : 'w-fit max-h-[650px] rounded-2xl object-cover transform transition-transform hover:scale-110';

  return (
    <div className={`${sectionClasses} p-4`} style={{ ...sectionStyle }}>
      <div className='w-full max-w-prose space-y-6 px-4'>
        <h3 className='text-4xl font-semibold text-azure'>{title}</h3>
        <h3 className='text-2xl font-semibold text-gray-800'>{subtitle}</h3>
        <p className='text-base text-gray-700'>{description}</p>
        { button? (
            buttonFull? (
              <Button theme='secondary'  className='bg-gradient-to-r from-sky-500 to-violet-500 hover:bg-gradient-to-r hover:from-sky-500 hover:to-violet-600' href={buttonLink}>{buttonText}</Button>
            ) : <p className='text-base font-semibold text-azure hover:text-violet-500'><a href={buttonLink}>{buttonText} →</a></p>
        ) : '' }
      </div>
      <div className='w-full lg:w-1/2 lg:px-4'>
        {images ? (
          <div className='slick-slider-wrapper'>
            <Slider {...settings}>
              {images.map((src, index) => (
                <div key={index}>
                  <animated.img style={props} className='w-full h-auto rounded-2xl object-cover transform transition-transform' src={src} alt={`${title}-screenshot-${index}`} />
                </div>
              ))}
            </Slider>
          </div>
        ) : (
          <animated.img style={props} className={imageClasses} src={image} alt={title} />
        )}
      </div>
    </div>
  );
};

export default FeatureSection;
