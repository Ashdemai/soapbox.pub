import clsx from 'clsx';
import React from 'react';

interface IMenuButton {
  active?: boolean
  onClick: React.MouseEventHandler<HTMLButtonElement>
}

/** Animated hamburger menu button. */
const MenuButton: React.FC<IMenuButton> = ({ active = false, onClick }) => {
  const baseClassName = 'origin-center transition-all';

  return (
    <button onClick={onClick}>
      <svg
        className='icon icon-tabler icon-tabler-menu-2 relative'
        width='24'
        height='24'
        viewBox='0 0 24 24'
        strokeWidth='2'
        stroke='currentColor'
        fill='none'
        strokeLinecap='round'
        strokeLinejoin='round'
      >
        <path
          className={clsx(baseClassName, { 'rotate-45 -translate-x-[4.25px] translate-y-[4.25px]': active })}
          d='M 4,6 H 20'
        />

        <path
          className={clsx(baseClassName, { '-rotate-45 opacity-0': active })}
          d='M 4,12 H 20'
        />

        <path
          className={clsx(baseClassName, { '-rotate-45 -translate-x-[4.25px] -translate-y-[4.25px]': active })}
          d='M 4,18 H 20'
        />
      </svg>
    </button>
  );
};

export default MenuButton;