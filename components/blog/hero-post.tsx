import Link from 'next/link';

import DateFormatter from '../date-formatter';

import Author from './author';
import CoverImage from './cover-image';

import type { Author as AuthorType } from 'contentlayer/generated';

type Props = {
  title: string
  coverImage: string
  date: string
  excerpt: string
  author: AuthorType
  slug: string
}

const HeroPost = ({
  title,
  coverImage,
  date,
  excerpt,
  author,
  slug,
}: Props) => {
  return (
    <section className='md:grid md:grid-cols-3 space-y-12 md:space-x-12 md:space-y-0'>
      <div className='col-span-2'>
        <CoverImage title={title} src={coverImage} slug={slug} />
      </div>

      <div className='col-span-1'>
        <div>
          <h3 className='mb-2 text-2xl lg:text-3xl leading-tight'>
            <Link
              as={`/blog/${slug}`}
              href='/blog/[slug]'
              className='hover:underline'
            >
              {title}
            </Link>
          </h3>

          <div className='mb-4 md:mb-0 text-lg text-gray-500'>
            <DateFormatter dateString={date} />
          </div>
        </div>

        <div>
          <p className='mb-4'>
            {excerpt.slice(0, 150)}&hellip;
          </p>

          <Author name={author.name} picture={author.picture} />
        </div>
      </div>
    </section>
  );
};

export default HeroPost;
