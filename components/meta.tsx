import Head from 'next/head';

import UnifiedMeta from './unified-meta';

const Meta = () => {
  return <>
    <Head>
      <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no' />
      <link
        rel='apple-touch-icon'
        sizes='180x180'
        href='/favicon/apple-touch-icon.png'
      />
      <link
        rel='icon'
        type='image/png'
        sizes='32x32'
        href='/favicon/favicon-32x32.png'
      />
      <link
        rel='icon'
        type='image/png'
        sizes='16x16'
        href='/favicon/favicon-16x16.png'
      />
      <link rel='manifest' href='/favicon/site.webmanifest' />
      <link
        rel='mask-icon'
        href='/favicon/safari-pinned-tab.svg'
        color='#000000'
      />
      <link rel='shortcut icon' href='/favicon/favicon.ico' />
      <meta name='msapplication-TileColor' content='#000000' />
      <meta name='msapplication-config' content='/favicon/browserconfig.xml' />
      <meta name='theme-color' content='#000' />
      <link rel='alternate' type='application/rss+xml' href='/rss/feed.xml' />
      <link rel='alternate' type='application/atom+xml' href='/rss/atom.xml' />
      <link rel='alternate' type='application/feed+json' href='/rss/feed.json' />
      <meta property='og:type' content='website' />
      <meta name='twitter:card' content='summary_large_image' />
    </Head>

    <UnifiedMeta
      title='Soapbox'
      description='Soapbox is customizable open-source software that puts the power of social media in the hands of the people.'
      image='https://soapbox.pub/assets/blog/releasing-soapbox-3.0/soapbox-readme-cover-full.png'
    />
  </>;
};

export default Meta;
